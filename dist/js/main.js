const burger = document.querySelector(".header-burger");
const burgerMenu = document.querySelector(".header-menu");

document.addEventListener("click", function(event) {
    if (event.target.closest(".header-burger")){
        burger.classList.toggle("active");
        burgerMenu.classList.toggle("active");
        document.body.classList.toggle("lock");
    }
})