let desktopArray = [
    { imgUrl: "./images/1200/post1.png"},
    { imgUrl: "./images/1200/post2.png"},
    { imgUrl: "./images/1200/post3.png"},
    { imgUrl: "./images/1200/post4.png"},
]

let tabletArray = [
    { imgUrl: "./images/768/post1.png"},
    { imgUrl: "./images/768/post2.png"},
    { imgUrl: "./images/768/post3.png"},
]

let mobilArray = [
    { imgUrl: "./images/320/rover.png"},
]


const instContainer = document.querySelector(".inst-container");
let instaItems = document.querySelectorAll(".insta-item");

function mobile(event) {
    for(let item of instaItems) {
        item.remove();
    }

    if(event.matches) {
      instContainer.innerHTML = '';
      for (let img of mobilArray) {
            let imgElement = document.createElement("img");
            imgElement.classList.add("insta-item");
            imgElement.src = img.imgUrl;
            instContainer.appendChild(imgElement);
        }
    }
}

function tablet(event) {
//remove existing photos
    
    for(let item of instaItems){
        item.remove();
    }
//adding photos
    if(event.matches) {
        instContainer.innerHTML = '';
        for (let img of tabletArray) {
            let imgElement = document.createElement("img");
            imgElement.classList.add("insta-item");
            imgElement.src = img.imgUrl;
            instContainer.appendChild(imgElement);
        }
    }
}

function desktop(event) {
    //remove existing photos
        
        for(let item of instaItems){
            item.remove();
        }
    //adding photos
        if(event.matches) {
            instContainer.innerHTML = '';
            for (let img of desktopArray) {
                let imgElement = document.createElement("img");
                imgElement.classList.add("insta-item");
                imgElement.src = img.imgUrl;
                instContainer.appendChild(imgElement);
            }
        }
    }


let underMobileQuery = window.matchMedia("(max-width: 320px)");
let mobileQuery = window.matchMedia("(min-width: 321px) and (max-width: 768px)");
let tabletQuery = window.matchMedia("(min-width: 769px) and (max-width: 1200px)");
let desktopQuery = window.matchMedia("(min-width: 1201px)");

mobileQuery.addListener(mobile);
tabletQuery.addListener(tablet);
desktopQuery.addListener(desktop);