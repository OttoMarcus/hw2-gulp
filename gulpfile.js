"use strict"
const {src, dest} = require("gulp")
const gulp = require("gulp")
const autoprefixer = require("gulp-autoprefixer")
const beautify = require("gulp-cssbeautify")
const cssnano = require("gulp-cssnano")
const rigger = require("gulp-rigger")
const sass = require('gulp-sass')(require('sass'));
const removeComments = require("gulp-strip-css-comments")
const rename = require("gulp-rename")
const uglify = require("gulp-uglify")
const plumber = require("gulp-plumber")
const del = require("del")
const notify = require("gulp-notify");
const browserSync = require("browser-sync").create();

// const { watch } = require("browser-sync");
// const { require } = require("gulp-cli/lib/shared/cli-options")


const srcPath = "src/";
const distPath = "dist/";

const path = {
    build: {
        html : distPath,
        css: distPath + "css/",
        js: distPath + "js/",
        images: distPath + "images/",
        fonts: distPath + "fonts/",
    },

    src: {
        html: srcPath + "html/*.html",
        css: srcPath + "scss/*.scss",
        js: srcPath + "js/*.js",
        images: srcPath + "images/**/*.{jpg,png,svg,gif,ico,webp,webmanifest,xml,json}",
        // fonts: srcPath + "fonts/**/*.{svg,ttf,wolf,wolf2,otf,eot"
    },


    watch: {
        html: srcPath + "html/**/*.html",
        css: srcPath + "scss/**/*.scss",
        js: srcPath + "js/**/*.js",
        images: srcPath + "images/**/*.{jpg,png,svg,gif,ico,webp,webmanifest,xml,json}",
        // fonts: srcPath + "fonts/**/*.{svg,ttf,wolf,wolf2,otf,eot"
    },

    clean : "./distPath"
}

function serve() {
    browserSync.init({
        server: {
            baseDir: "./" + distPath
        },
        injectChanges: true
    })
}

function html() {
    return src(path.src.html, {base: srcPath + "html/"})
    .pipe(plumber())
    .pipe(dest(path.build.html))
    .pipe(browserSync.reload({stream: true}))
}

function css() {
    return src(path.src.css, {base: srcPath + "scss/"})
    .pipe(plumber())
    .pipe(sass().on("error", sass.logError))
    .pipe(autoprefixer({
            overrideBrowserslist: ['last 2 versions'],
            cascade: false
        }))
    .pipe(beautify())
    .pipe(dest(path.build.css))
    .pipe(cssnano({
        zindex: false,
        discardComments: {
            removeAll: true
        }
    }))
    .pipe(removeComments())
    .pipe(rename({
        suffix: ".min",
        extname: ".css"
    }))
    .pipe(dest(path.build.css))
    .pipe(browserSync.reload({stream: true}))
}

function js() {
    return src(path.src.js, {base: srcPath + "js/"})
    .pipe(plumber({
        errorHandler : function(err) {
            notify.onError({
                title: "JS Error",
                message: "Error:<% - error.message %>"
            })(err);
            this.emit('end');
        }
}))
    .pipe(rigger())
    .pipe(dest(path.build.js))
    .pipe(uglify())
    .pipe(rename({
        suffix: ".min",
        extname: ".js"
    }))
    .pipe(dest(path.build.js))
    .pipe(browserSync.reload({stream: true}))
}

function images() {
    return src(path.src.images, {base: srcPath + "images/"})
    .pipe(dest(path.build.images))
    .pipe(browserSync.reload({stream: true}))
}

function clean() {
    return del(path.clean)
}

// function fonts() {
//     return src(path.src.fonts, {base: srcPath + "fonts/"})
//     .pipe(browserSync.reload({stream: true}))
// }

function watchFiles() {
    gulp.watch([path.watch.html], html)
    gulp.watch([path.watch.css], css)
    gulp.watch([path.watch.js], js)
    gulp.watch([path.watch.images], images)
    // gulp.watch([path.watch.fonts], fonts)
}

const build = gulp.series(clean, gulp.parallel(html, css, js, images))
const watch = gulp.series(build, watchFiles, serve)

exports.html = html
exports.css = css
exports.js = js
exports.images = images
exports.clean = clean
// exports.fonts = fonts
exports.build = build
exports.watch = watch
exports.default = watch